import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)


import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import Dashboard from '../views/Dashboard.vue'



const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {guestOnly: true}
    },
    {
        path: '/register',
        name: 'register',
        component: Register,
        meta: {guestOnly: true}
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard,
        meta: {requiresAuth: true} // only access who are authticated
    }
]

const router  = new VueRouter({
    routes,
    mode: 'history'
})

function isAuthticated(){
    return localStorage.getItem('token');
}


router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {

      if (!isAuthticated()) {
        next({
          path: '/login',
          query: { redirect: to.fullPath }
        })
      } else {
        next()
      }

    } else if(to.matched.some(record => record.meta.guestOnly)){
        if (isAuthticated()) {
            next({
              path: '/dashboard',
              query: { redirect: to.fullPath }
            })
          } else {
            next()
          }
    }else {
      next() // make sure to always call next()!
    }
  })



export default router;



