import Vue from 'vue'
import App from './App.vue'


import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'


Vue.config.productionTip = false


import router from './router/router'

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
