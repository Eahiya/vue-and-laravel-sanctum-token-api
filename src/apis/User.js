import Api from './Api'

export default {
    register(form_data){
        return Api().post('/register', form_data)
    },
    login(form_data){
        return Api().post('/login',  form_data)
    },
    logout(){
        return Api().post('/logout')
    },
    auth(){
        return Api().get('/user')
    }
}

